﻿ideology_eordand_unification_movement = {
	icon = "gfx/interface/icons/ideology_icons/sovereignist.dds"

	lawgroup_church_and_state = {
		law_state_religion = neutral
		law_freedom_of_conscience = strongly_approve
		law_total_separation = disapprove
		law_state_atheism = strongly_disapprove
	}
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = disapprove
		law_racial_segregation = approve
		law_cultural_exclusion = neutral
		law_multicultural = disapprove
	}
	
	lawgroup_migration = {
		law_closed_borders = strongly_disapprove
		law_migration_controls = approve
		law_no_migration_controls = neutral
	}
}
ideology_pluralist_movement_3 = { # Cultural Exclusion
	icon = "gfx/interface/icons/ideology_icons/reformer.dds"
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = strongly_disapprove
		law_racial_segregation = disapprove
		law_cultural_exclusion = approve
		law_multicultural = neutral
	}
	lawgroup_racial_tolerance = {
		law_same_heritage_only = strongly_disapprove
		law_local_tolerance = approve
		law_expanded_tolerance = strongly_approve
		law_all_heritage = neutral
	}
}
ideology_pluralist_movement_4 = { # Multicultural
	icon = "gfx/interface/icons/ideology_icons/reformer.dds"
	
	lawgroup_citizenship = {
		law_ethnostate = strongly_disapprove
		law_national_supremacy = strongly_disapprove
		law_racial_segregation = disapprove
		law_cultural_exclusion = disapprove
		law_multicultural = approve
	}
	lawgroup_racial_tolerance = {
		law_same_heritage_only = strongly_disapprove
		law_local_tolerance = strongly_disapprove
		law_expanded_tolerance = approve
		law_all_heritage = strongly_approve
	}
}

ideology_magocratic_movement = {
	icon = "gfx/interface/icons/ideology_icons/unused/theocrat.dds"

		lawgroup_governance_principles = {
		law_magocracy = approve	
		law_stratocracy = disapprove
		law_monarchy = neutral
		law_theocracy = neutral	
		law_presidential_republic = disapprove
		law_parliamentary_republic = disapprove
		law_council_republic = disapprove
	}
	
	lawgroup_magic_and_artifice = {
		law_artifice_banned = strongly_approve
		law_nation_of_magic = approve
		law_nation_of_artifice = disapprove
		law_traditional_magic_banned = strongly_disapprove
	}
	
	lawgroup_economic_system = {
		law_cooperative_ownership = neutral
		law_command_economy = disapprove
		law_interventionism = disapprove
		law_agrarianism = approve
		law_industry_banned = approve
		law_traditionalism = approve		
		law_laissez_faire = disapprove		
	}
}