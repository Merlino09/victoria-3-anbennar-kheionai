﻿CHARACTERS = {
	c:A01 ?= {
		# create_character = {
		# 	first_name = Artur_Laurens
		# 	last_name = Silmuna
		# 	historical = yes
		# 	ruler = yes
		# 	age = 10
		# 	birth_date = 1826.2.4
		# 	interest_group = ig_intelligentsia
		# 	ideology = ideology_reformer
		# 	dna = dna_artur_laurens_silmuna
		# 	traits = {
		# 		charismatic demagogue
		# 	}
		# }

		#book dude
		create_character = {
			first_name = Rean
			last_name = sil_Tirufeld
			noble = yes
			historical = yes
			age = 33	#born 1786
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_humanitarian_royalist
			dna = dna_rean_sil_tirufeld
			traits = {
				tactful expert_political_operator
			}
		}

		create_character = {
			is_general = yes
			first_name = Artura
			last_name = Loxara
			historical = yes
			female = yes
			age = 28
			interest_group = ig_armed_forces
			ideology = ideology_royalist
			hq = region_eastern_dameshead
			commander_rank = commander_rank_2
			traits = {
				imperious traditionalist_commander
			}
		}

		create_character = { 
			first_name = Delda
			last_name = Waggleknack
			historical = yes
			is_agitator = yes
			female = yes
			age = 40	#need to fix years eventually
			interest_group = ig_intelligentsia
			ideology = ideology_radical
			traits = {
				meticulous literary
			}
			culture = cu:imperial_gnome
			religion = rel:ravelian
		}






		#Aakhet
		create_character = {
			first_name = aakhet
			last_name = the_bronze
			historical = yes
			culture = cu:vernman	#cu:tijarikheti
			age = 42
			is_agitator = yes
			interest_group = ig_armed_forces
			ideology = ideology_aakhet
			traits = { #no actual immortal trait because its supposed to be a secret
				arrogant
				ambitious
				direct
				expert_offensive_planner
			}
			on_created = {
				set_character_immortal = yes
				add_character_role = general
				add_commander_rank = 1
			}
		}
	}
}
