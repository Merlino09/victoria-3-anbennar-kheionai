﻿MILITARY_FORMATIONS = {
	c:R20 ?= { # Raghamidesh
		create_military_formation = {
			type = army
			hq_region = sr:region_south_rahen
			name = "Raghamidesh Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_AVHAVUBHIYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_AVHAVUBHIYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_AVHAVUBHIYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_IYARHASHAR
				count = 10
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_south_rahen
			name = "Raghamidesh Fleet"
		
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_AVHAVUBHIYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_AVHAVUBHIYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_IYARHASHAR
				count = 5
			}
		}
	}
	
	c:R02 ?= { # Ghankedhen
		create_military_formation = {
			type = army
			hq_region = sr:region_south_rahen
			name = "Ghankedhen Rangers"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SOUTH_GHANKEDHEN
				count = 1
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SOUTH_GHANKEDHEN
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NORTH_GHANKEDHEN
				count = 2
			}
		}
	}
	
	c:R01 ?= { # Nahana Jadd
		create_military_formation = {
			type = army
			hq_region = sr:region_south_rahen
			name = "Eastern Legion"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_LOWER_DHENBASANA
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SATARSAYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_PASIRAGHA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_UPPER_DHENBASANA
				count = 25
			}
		}
	}
	
	c:R03 ?= { # Tudhina
		create_military_formation = {
			type = army
			hq_region = sr:region_south_rahen
			name = "Tudhina Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_TUDHINA
				count = 5
			}
		}
	}
	
	c:R04 ?= { # Vanrahar
		create_military_formation = {
			type = army
			hq_region = sr:region_south_rahen
			name = "Harimar's Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_ASCENSION_JUNGLE
				count = 12
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TUJGAL
				count = 10
			}
		}
	}
	
	c:R06 ?= { # Babhagama
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Babhagama Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_BABHAGAMA
				count = 12
			}
		}
	}
	
	c:R05 ?= { # Bhuvauri
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "Dragon Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SRAMAYA
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SRAMAYA
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_SRAMAYA
				count = 2
			}
		}

		create_military_formation = {
			type = fleet
			hq_region = sr:region_kharunyana
			name = "Brass Fleet"
		
			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_SRAMAYA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_SRAMAYA
				count = 10
			}
		}
	}
	
	c:R10 ?= { # Khiraspid
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "River Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DHUJAT
				count = 5
			}
		}
	}
	
	c:R11 ?= { # Keyattordha
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "River Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DHUJAT
				count = 5
			}
		}
	}
	
	c:Y13 ?= { # Phanbang
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "River Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KHARUNYANA_BOMDAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_KHARUNYANA_BOMDAN
				count = 2
			}
		}
	}
	
	c:Y12 ?= { # Semphrerong
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "River Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KHARUNYANA_BOMDAN
				count = 15
			}
		}
	}
	
	c:R12 ?= { # Maharaaja
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Maharaaja Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EAST_GHAVAANAJ
				count = 8
			}
		}
	}
	
	c:R72 ?= { # Dhugajir
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Dhugajir Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EAST_GHAVAANAJ
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_EAST_GHAVAANAJ
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_WEST_GHAVAANAJ
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_WEST_GHAVAANAJ
				count = 10
			}
		}
	}
	
	c:R09 ?= { # Pordhatti
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_TUGHAYASA
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_TUGHAYASA
				count = 5
			}
		}
	}
	
	c:R07 ?= { # Nadiraji
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "Royal Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_WEST_NADIMRAJ
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_RAJNADHAGA
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_TILTAGHAR
				count = 5
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "River Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_CENTRAL_NADIMRAJ
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EAST_NADIMRAJ
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_CENTRAL_NADIMRAJ
				count = 5
			}
		}
	}
	
	c:R13 ?= { # Yodhashikyu
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "Oracle Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_TUGHAYASA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TUGHAYASA
				count = 15
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "Sarisung Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SARISUNG
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_QIANZHAOLIN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_QIANZHAOLIN
				count = 5
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_west_yanshen
			name = "Xiaken Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_JIEZHONG
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_WANGQIU
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_WANGQIU
				count = 10
			}
		}
	}
	
	c:R14 ?= { # Girakhad
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GHILAKHAD
				count = 25
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_GHILAKHAD
				count = 5
			}
		}
	}
	
	c:R15 ?= { # Unbroken Guard
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SHAMAKHAD_PLAINS
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SHAMAKHAD_PLAINS
				count = 5
			}
		}
	}
	
	c:R16 ?= { # Kasardun
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SHAMAKHAD_PLAINS
				count = 16
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SHAMAKHAD_PLAINS
				count = 4
			}
		}
	}
	
	c:R18 ?= { # Bureau of Chains
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SHAMAKHAD_PLAINS
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SHAMAKHAD_PLAINS
				count = 5
			}
		}
	}
	
	c:R17 ?= { # Lion Command
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "1st Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GHATASAK
				count = 30
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_GHATASAK
				count = 10
			}
		}
	}
	
	c:R08 ?= { # Soldiers Republic
		create_military_formation = {
			type = army
			hq_region = sr:region_north_rahen
			name = "Great Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_RAGHAJANDI
				count = 25
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_RAGHAJANDI
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_RAGHAJANDI
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HOBGOBLIN_HOMELANDS
				count = 25
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_HOBGOBLIN_HOMELANDS
				count = 10
			}
		}
	}
	
	c:Y09 ?= { # Azjakuma
		create_military_formation = {
			type = army
			hq_region = sr:region_north_yanshen
			name = "Great Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SUGROLTONA
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_MORYOKANG
				count = 15
			}
		}
	}
	
	c:R19 ?= { # Dragon Command
		create_military_formation = {
			type = army
			hq_region = sr:region_west_yanshen
			name = "Sir Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_SIR
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_SIR
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_MOGUTIAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_MOGUTIAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_BIANYUAN
				count = 5
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_west_yanshen
			name = "Dragon Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BIANYUAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_BIANYUAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_BIANYUAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_EBYANFANGU
				count = 25
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_EBYANFANGU
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_YANSZIN
				count = 10
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_west_yanshen
			name = "Wuhyun Kikun"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_BIANYUAN
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_BIANYUAN
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_BIANYUAN
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_EBYANFANGU
				count = 30
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_YANSZIN
				count = 20
			}
		}
	}
	
	c:Y11 ?= { # Shuvuushdi
		create_military_formation = {
			type = army
			hq_region = sr:region_north_yanshen
			name = "1st Shuvuush Banner"

			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_YUNGHUUN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_YUNGHUUN
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_JIANTSIANG
				count = 25
			}
		}
		
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "2nd Shuvuush Banner"

			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_YUNGHUUN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_LIUMINXIANG
				count = 20
			}
		}
		
		create_military_formation = {
			type = fleet
			hq_region = sr:region_north_yanshen
			name = "Shuvuush Fleet"

			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_XUANBING
				count = 5
			}
		}
	}
	
	c:Y08 ?= { # Xuanbing
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "1nd Xuanbing Banner"

			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_XUANBING
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_XUANBING
				count = 15
			}
		}
	}
	
	c:Y03 ?= { # Tianlou
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "1nd Tianlou Banner"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_XUANBING
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_TIANLOU
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_TIANLOU
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_JINJIANG
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_JINJIANG
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_LIUMINXIANG
				count = 15
			}
		}
		
		create_military_formation = {
			type = fleet
			hq_region = sr:region_north_yanshen
			name = "1st Tianlou Fleet"

			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_TIANLOU
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_TIANLOU
				count = 10
			}
		}
	}
	
	c:Y04 ?= { # Lingyuk
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "1nd Tail"

			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_LINGYUK
				count = 5
			}
		}
	}
	
	c:Y05 ?= { # Feiten
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "1nd Feiten Army"

			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_IONGSIM
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HUNGNGON
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_TIANLOU
				count = 5
			}
		}
		
		create_military_formation = {
			type = fleet
			hq_region = sr:region_north_yanshen
			name = "1st Jellyfish Fleet"

			combat_unit = {
				type = unit_type:combat_unit_type_man_o_war
				state_region = s:STATE_IONGSIM
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_IONGSIM
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_frigate
				state_region = s:STATE_HUNGNGON
				count = 5
			}
		}
	}
	
	c:Y06 ?= { # Luoyip
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "1nd Luoyip Banner"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ZYUJYUT
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_LUOYIP
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_LUOYIP
				count = 5
			}
		}
	}
	
	c:Y07 ?= { # Zyujyut
		create_military_formation = {
			type = army
			hq_region = sr:region_east_yanshen
			name = "1st Zyujyut Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ZYUJYUT
				count = 10
			}
		}
	}
	
	c:Y14 ?= { # Taiver
		create_military_formation = {
			type = army
			hq_region = sr:region_bomdan
			name = "1st Taiver Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_BIM_LAU
				count = 20
			}
		}
	}
	
	c:Y15 ?= { # Bim Lau
		create_military_formation = {
			type = army
			hq_region = sr:region_bomdan
			name = "Necropolis Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_BIM_LAU
				count = 20
			}
		}
	}
	
	c:Y16 ?= { # Khabtei Teleni
		create_military_formation = {
			type = army
			hq_region = sr:region_bomdan
			name = "Necropolis Guard"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KHABTEI_TELENI
				count = 15
			}
		}
	}
	
	c:Y17 ?= { # Azkare
		create_military_formation = {
			type = army
			hq_region = sr:region_thidinkai
			name = "United Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_DEKPHRE
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_SIKAI
				count = 5
			}
		}
	}
	
	c:Y18 ?= { # Rongbek
		create_military_formation = {
			type = army
			hq_region = sr:region_bomdan
			name = "1st Rongbek Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_RONGBEK
				count = 5
			}
		}
	}
	
	c:Y19 ?= { # Baihon Xinh
		create_military_formation = {
			type = army
			hq_region = sr:region_thidinkai
			name = "Royal Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KHOM_MA
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_KHOM_MA
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_PHONAN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_PHONAN
				count = 10
			}
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "Coastal Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_HOANGDESINH
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_HOANGDESINH
				count = 5
			}
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_bomdan
			name = "Bom Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NAGON
				count = 15
			}
		}
	}
	
	c:Y20 ?= { # Verkal Ozovar
		create_military_formation = {
			type = army
			hq_region = sr:region_thidinkai
			name = "Nephrite Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_VERKAL_OZOVAR
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_VERKAL_OZOVAR
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_HINPHAT
				count = 15
			}
		}
	}
	
	c:Y21 ?= { # Kudet Kai
		create_military_formation = {
			type = army
			hq_region = sr:region_thidinkai
			name = "Kai Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_KUDET_KAI
				count = 5
			}
		}
	}
	
	c:Y22 ?= { # Sirtan
		create_military_formation = {
			type = army
			hq_region = sr:region_thidinkai
			name = "Desert Raiders"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_SIRTAN
				count = 6
			}
			combat_unit = {
				type = unit_type:combat_unit_type_hussars
				state_region = s:STATE_SIRTAN
				count = 6
			}
		}
	}
	
	c:Y23 ?= { # Arawkelin
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "1st Arawkelin Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_ARAWKELIN
				count = 25
			}
		}
	
	
	c:Y24 ?= { # Jaya Raya
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "1st Jaya Raya Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_TLAGUKIT
				count = 12
			}
		}
	}
	
	c:Y25 ?= { # Pelimkana
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "1st Pelimkana Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_REWIRANG
				count = 2
			}
		}
	}
	
	c:Y30 ?= { # Hijoadan
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "1st Hijoadan Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_MESATULEK
				count = 1
			}
		}
	}
	
	c:Y31 ?= { # Labanyak
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "1st Labanyak Army"

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_MESATULEK
				count = 1
			}
		}
	}
	
	c:Y32 ?= { # LHC
		create_military_formation = {
			type = army
			hq_region = sr:region_bomdan
			name = "1st Company Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_LOWER_TELEBEI
				count = 20
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_KHINDI
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_KHINDI
				count = 5
			}
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_kharunyana
			name = "2nd Company Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_KHARUNYANA_BOMDAN
				count = 15
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_KHARUNYANA_BOMDAN
				count = 5
			}
		}
	}
	
	c:Y33 ?= { # BHC
		create_military_formation = {
			type = army
			hq_region = sr:region_lupulan
			name = "1st Company Army"

			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_NON_CHIEN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_NON_CHIEN
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_NON_CHIEN
				count = 2
			}
		}
		create_military_formation = {
			type = army
			hq_region = sr:region_thidinkai
			name = "2nd Company Army"

			combat_unit = {
				type = unit_type:combat_unit_type_cuirassiers
				state_region = s:STATE_KUDET_KAI
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_YEMAKAIBO
				count = 5
			}
			combat_unit = {
				type = unit_type:combat_unit_type_line_infantry
				state_region = s:STATE_BINHRUNGHIN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_BINHRUNGHIN
				count = 5
			}
		}
	}
	c:Y39 ?= { # Nirenan syul
		create_military_formation = {
			type = army
			hq_region = sr:region_nomsyulhan

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_UPPER_KHARUNYANA_BASIN
				count = 10
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_UPPER_KHARUNYANA_BASIN
				count = 2
			}
			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_GREENVALE
				count = 4
			}
			combat_unit = {
				type = unit_type:combat_unit_type_cannon_artillery
				state_region = s:STATE_GREENVALE
				count = 3
			}
		}
	}
	c:Y40 ?= { # Samthalsu
		create_military_formation = {
			type = army
			hq_region = sr:region_nomsyulhan

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_NOMSYULHANI_BADLANDS
				count = 6
			}
		}
	}
	c:Y41 ?= { # Hakuukulga
		create_military_formation = {
			type = army
			hq_region = sr:region_nomsyulhan

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_HAKUURUK
				count = 4
			}
		}
	}
	c:Y43 ?= { # Bazuneizar
		create_military_formation = {
			type = army
			hq_region = sr:region_nomsyulhan

			combat_unit = {
				type = unit_type:combat_unit_type_irregular_infantry
				state_region = s:STATE_HAKUURUK
				count = 1
			}
		}
	}
}