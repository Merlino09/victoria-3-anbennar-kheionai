﻿COUNTRIES = {
	c:F08 ?= {
		effect_starting_technology_tier_3_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		
		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_magic
	}
}