﻿COUNTRIES = {
	c:B48 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = napoleonic_warfare
		add_technology_researched = empiricism

		set_institution_investment_level = {
			institution = institution_police #Rangers, as per the name
			level = 2
		}
	}
}