﻿COUNTRIES = {
	c:A04 ?= {
		effect_starting_technology_tier_1_tech = yes
		effect_starting_artificery_tier_2_tech = yes

		effect_starting_politics_conservative = yes

		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_private_schools

		activate_law = law_type:law_professional_army
		activate_law = law_type:law_laissez_faire
		activate_law = law_type:law_protectionism
		activate_law = law_type:law_colonial_exploitation

		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_poor_laws
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_nation_of_artifice
		
		activate_law = law_type:law_same_heritage_only
		
		# set_tax_level = high

		add_taxed_goods = g:liquor
		add_taxed_goods = g:clothes

		set_ruling_interest_groups = {
			ig_intelligentsia ig_industrialists ig_armed_forces
		}

		ig:ig_armed_forces = {
			set_interest_group_name = ig_old_guard
		}

		ig:ig_industrialists = {
			set_interest_group_name = ig_trade_barons
			set_ig_bolstering = yes
		}

		ig:ig_intelligentsia = {
			set_interest_group_name = ig_cooperatists
		}

		set_institution_investment_level = {
			institution = institution_colonial_affairs
			level = 2
		}

		set_institution_investment_level = {
			institution = institution_schools
			level = 2
		}

		#add_journal_entry = { type = je_industrialization_of_the_league }
		#set_variable = { name = nl_construction_var value = 0 }
	}
}