﻿COUNTRIES = {
	c:B82 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = lathe
		add_technology_researched = tradition_of_equality

		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		# No colonial affairs
		# No police
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery

		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_magic
	}
}