﻿COUNTRIES = {
	c:F01 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_theocracy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_cultural_exclusion #Multiculturalism not possible without human rights
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_local_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property

		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_artifice_banned


		ig:ig_devout = {
			add_ruling_interest_group = yes
		}
		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
	}
}