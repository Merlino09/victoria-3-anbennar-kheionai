﻿COUNTRIES = {
	c:B03= {
		effect_starting_technology_tier_2_tech = yes
		effect_starting_artificery_tier_2_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy	#board?
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_total_separation
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_industry_banned
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_colonial_resettlement
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_no_schools
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slave_trade

		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
	}
}