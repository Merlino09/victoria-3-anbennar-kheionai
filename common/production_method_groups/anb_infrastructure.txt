﻿pmg_base_building_arcane_nexus = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_traditional_mage_tower
		pm_traditional_technodruid_tower
		pm_arcane_scholar_network
		pm_druidic_scholar_network
		pm_masters_of_magic
		pm_masters_of_technothaumaturgie
		pm_modern_mages
		pm_modern_technodruid
	}
}

pmg_transmuation_building_arcane_nexus = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_transmutation_building_arcane_nexus
		pm_distilled_rainbows_building_arcane_nexus
		pm_manaspun_fibers_building_arcane_nexus
		pm_thaumosplastic_rendering_building_arcane_nexus
	}
}